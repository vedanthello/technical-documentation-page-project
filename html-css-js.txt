HTML body:
<script src="https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js">
</script>
<nav id="navbar">
  <header id="navbar-header">
    <h1>JS Documentation<h1>
  </header>  
  <ul>
    <li><a href="#Introduction" class="nav-link">Introduction</a>
    </li>
    <li><a href="#Declaring_variables" class="nav-link">Declaring variables</a>
    </li>  
    <li><a href="#Global_variables" class="nav-link">Global variables</a>
    </li>  
    <li><a href="#while_statement" class="nav-link">while statement</a>
    </li>  
    <li><a href="#Reference" class="nav-link">Reference</a>
    </li>      
  </ul>  
</nav>
<main id="main-doc">
  <section class="main-section" id="Introduction">
    <header>
      <h1>Introduction</h1>
    </header>  
    <article>
      <p>
        JavaScript is a cross-platform, object-oriented scripting language. It is a small and lightweight language. Inside a host environment (for example, a web browser), JavaScript can be connected to the objects of its environment to provide programmatic control over them.
      </p>
      <p>
        JavaScript contains a standard library of objects, such as Array, Date, and Math, and a core set of language elements such as operators, control structures, and statements. Core JavaScript can be extended for a variety of purposes by supplementing it with additional objects; for example:
      </p>
      <ul>
        <li>
          Client-side JavaScript extends the core language by supplying objects to control a browser and its Document Object Model (DOM). For example, client-side extensions allow an application to place elements on an HTML form and respond to user events such as mouse clicks, form input, and page navigation.
        </li>
        <li>
          Server-side JavaScript extends the core language by supplying objects relevant to running JavaScript on a server. For example, server-side extensions allow an application to communicate with a database, provide continuity of information from one invocation to another of the application, or perform file manipulations on a server.
        </li>  
      </ul>  
    </article>
  </section>
  <section class="main-section" id="Declaring_variables">
    <header>
      <h1>Declaring variables<h1>
    </header>
    <article>
      <p>
        You can declare a variable in three ways:
      </p>
      <p>
        With the keyword var. For example,
      </p>
      <code class="code">
        var x = 42.
      </code>
      <p>
        This syntax can be used to declare both local and global variables.
      </p>
      <p>
        By simply assigning it a value. For example,
      </p>  
      <code class="code">
        x = 42.
      </code> 
      <p>
        This always declares a global variable. It generates a strict JavaScript warning. You shouldn't use this variant.
      </p>  
      <p>
        With the keyword let. For example,
      </p>  
      <code class="code">
         let y = 13.
      </code>
      <p>This syntax can be used to declare a block scope local variable. See Variable scope below.
      </p>  
    </article>  
  </section>  
  <section class="main-section" id="Global_variables">
    <header>
      <h1>Global variables</h1>
    </header>  
    <article>
      <p>
        Global variables are in fact properties of the global object. In web pages the global object is window, so you can set and access global variables using the window.variable syntax.
      </p>  
      <p>
        Consequently, you can access global variables declared in one window or frame from another window or frame by specifying the window or frame name. For example, if a variable called phoneNumber is declared in a document, you can refer to this variable from an iframe as parent.phoneNumber.
      </p>  
    </article>  
  </section>  
  <section class="main-section" id="while_statement">
    <header>
      <h1>while statement</h1>
    </header>  
    <article>
      <p>
        A while statement executes its statements as long as a specified condition evaluates to true. A while statement looks as follows:
      </p>  
      <code class="code">
        while (condition) statement
      </code>  
      <p>
        If the condition becomes false, statement within the loop stops executing and control passes to the statement following the loop.
      </p>  
      <p>
        The condition test occurs before statement in the loop is executed. If the condition returns true, statement is executed and the condition is tested again. If the condition returns false, execution stops and control is passed to the statement following while.
      </p>  
      <p>
        To execute multiple statements, use a block statement ({ ... }) to group those statements.
      </p>  
      <p>
        Example:
      </p>  
      <p>
        The following while loop iterates as long as n is less than three:
      </p> 
      <code class="code">
        var n = 0;<br>
        var x = 0;<br>
        while (n &lt 3) {<br>
        n++;<br>
        x += n;<br>
        }
      </code> 
       <p>
          With each iteration, the loop increments n and adds that value to x. Therefore, x and n take on the following values:
      </p>
      <ul>
        <li>After the first pass: n = 1 and x = 1
        </li>
        <li>After the second pass: n = 2 and x = 3
        </li>
        <li>After the third pass: n = 3 and x = 6
        </li>
      </ul> 
      <p>After completing the third pass, the condition n &lt 3 is no longer true, so the loop terminates.
      </p>  
    </article>  
  </section> 
  <section class="main-section" id="Reference">
    <header>
      <h1>Reference</h1>
    </header>  
    <article>
      <ul>
        <li>All the documentation in this page is taken from <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide">MDN</a>
        </li>
      </ul>  
    </article>  
  </section>  
</main> 

CSS:
.code {
  background-color: #eee;
  padding: 0.5em;
  border-radius: 5px;
  display: block;
}
@media (min-width: 700px) {
  #main-doc {
    margin-left: 13em;
    padding: 2em;
  }
  #navbar {
    position: fixed;
    top: 0;
    width: 13em;
    height: 100%;
    border-right: 1px solid #000;
  }
  #navbar-header {
    padding-top: 1em;
  }
}
@media (max-width: 700px) {
  #navbar-header {
    text-align: center;
  }
}

JavaScript:
(none).

Status:
All tests passed.
